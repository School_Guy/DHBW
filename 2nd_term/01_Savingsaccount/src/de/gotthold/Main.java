package de.gotthold;

import java.util.Random;

public class Main {
    int threads = 5000;
    Thread[] einzahler = new Thread[threads];
    Thread[] auszahler = new Thread[threads];
    int gesamtEinzahlen = 0;
    int gesamtAuszahlen = 0;
    final SavingsAccount account = new SavingsAccount();

    public static void main(String[] args) {
        Main main = new Main();
        main.initAccount();
        main.CreateThreads();
        main.RunThreads();
    }

    public void initAccount() {
        try {
            account.deposit(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void CreateThreads() {
        for (int i = 0; i < threads; i++) {
            einzahler[i] = new Thread() {
                public void run() {
                    try {
                        int rnd = new Random().nextInt(50);
                        gesamtEinzahlen = gesamtEinzahlen + rnd;
                        account.deposit(rnd);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //System.out.println("Guthaben: " + account.getBalance());
                }
            };
        }
        for (int i = 0; i < threads; i++) {
            auszahler[i] = new Thread() {
                public void run() {
                    try {
                        int rnd = new Random().nextInt(50);
                        gesamtAuszahlen = gesamtAuszahlen + rnd;
                        account.withdraw(rnd);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //System.out.println("Guthaben: " + account.getBalance());
                }
            };
        }
    }

    public void RunThreads() {
        int diff = (gesamtEinzahlen - gesamtAuszahlen);
        System.out.println("Gesamt Einzahlungen: " + gesamtEinzahlen
                + " Gesamt Auszahlungen: " + gesamtAuszahlen
                + " Differenz: " + diff);
        for (int i = 0; i< threads;i++) {
            einzahler[i].start();
            auszahler[i].start();
        }
        System.out.println("----------------------------Alle Threads gestartet.---------------------------------------");
    }
}
