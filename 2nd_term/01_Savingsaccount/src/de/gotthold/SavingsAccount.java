package de.gotthold;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

class SavingsAccount {
    private static final float BALANCE_MAX = 200000;
    private float balance;
    private ReentrantLock lock = new ReentrantLock();
    private Condition notEmptyCondition = lock.newCondition();
    private Condition emptyCondition = lock.newCondition();

    public SavingsAccount() {
        this.balance = 0;
    }

    synchronized public float getBalance() {
        return this.balance;
    }

    synchronized public void withdraw(float amount) throws InterruptedException {
        lock.lock();
        System.out.println("Lock gegeben.");
        try {
            while (this.balance < amount) {
                System.out.println("Warten. - Withdraw - " + amount);
                notEmptyCondition.await();
            }
            System.out.println("Auszahlen - Amount: " + amount + " Altes Guthaben: " + this.balance + " Neues Guthaben: " + (this.balance - amount));
            this.balance -= amount;
            emptyCondition.signalAll();
        } finally {
            System.out.println("Lock freigegeben.");
            lock.unlock();
        }
    }

    synchronized public void deposit(float amount) throws InterruptedException {
        lock.lock();
        System.out.println("Lock gegeben.");
        try {
            while (this.balance + amount > BALANCE_MAX) {
                System.out.println("Warten. - Deposit - " + amount);
                emptyCondition.await();
            }
            System.out.println("Einzahlen - Amount: " + amount + " Altes Guthaben: " + this.balance + " Neues Guthaben: " + (this.balance + amount));
            this.balance += amount;
            notEmptyCondition.signalAll();
        } finally {
            System.out.println("Lock freigegeben.");
            lock.unlock();
        }
    }
}
