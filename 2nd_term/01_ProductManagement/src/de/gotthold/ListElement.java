package de.gotthold;

public abstract class ListElement {
    abstract ListElement getSuccessor();
    abstract void addRear(Product p);
    abstract void delete(Product p);
    abstract boolean contains(Product p);
    abstract void printList();
    abstract Product getProduct();
}
