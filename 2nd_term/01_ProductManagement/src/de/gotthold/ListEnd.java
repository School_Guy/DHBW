package de.gotthold;

public class ListEnd extends ListElement {

    @Override
    ListElement getSuccessor() {
        return null;
    }

    @Override
    void addRear(Product p) {
        //Nothing to do
    }

    @Override
    void delete(Product p) {
        //Nothing to do
    }

    @Override
    boolean contains(Product p) {
        return false;
    }

    @Override
    void printList() {
        //Nothing to Print
    }

    @Override
    Product getProduct() {
        return null;
    }
}
