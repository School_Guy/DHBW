package de.gotthold;

public class ListKnot extends ListElement {
    private Product information;
    private ListElement successor;

    public ListKnot(Product p) {
        information = p;
    }

    void setSuccessor(ListElement element) {
        successor = element;
    }

    @Override
    ListElement getSuccessor() {
        return successor;
    }

    @Override
    void addRear(Product p) {
        if (successor.getProduct() == null) {
            ListKnot knot = new ListKnot(p);
            knot.setSuccessor(successor);
            successor = knot;
        } else {
            successor.addRear(p);
        }
    }

    @Override
    void delete(Product p) {
        if (!(successor.getProduct().equals(p))) {
            successor = successor.getSuccessor();
        } else {
            successor.delete(p);
        }
    }

    @Override
    boolean contains(Product p) {
        if (information.equals(p)) {
            return true;
        } else {
            return successor.contains(p);
        }
    }

    @Override
    void printList() {
        System.out.println("Product: " + information.print());
        successor.printList();
    }

    @Override
    Product getProduct() {
        return information;
    }
}
