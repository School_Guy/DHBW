package de.gotthold;

public class Main {

    public static void main(String[] args) {
        //Init List
        ProductList list = new ProductList();

        //Init Products
        Product p1 = new Product(String.valueOf("Product1".hashCode()),"Product1",0,Boards.BOARD1);
        Product p2 = new Product(String.valueOf("Product2".hashCode()),"Product2",5,Boards.BOARD1);
        Product p3 = new Product(String.valueOf("Product3".hashCode()),"Product3",8,Boards.BOARD1);
        Product p4 = new Product(String.valueOf("Product4".hashCode()),"Product4",10,Boards.BOARD1);
        Product p5 = new Product(String.valueOf("Product5".hashCode()),"Product5",13,Boards.BOARD1);
        Product p6 = new Product(String.valueOf("Product6".hashCode()),"Product6",2,Boards.BOARD1);
        Product p7 = new Product(String.valueOf("Product7".hashCode()),"Product7",9,Boards.BOARD1);
        Product p8 = new Product(String.valueOf("Product8".hashCode()),"Product8",4,Boards.BOARD1);
        Product p9 = new Product(String.valueOf("Product9".hashCode()),"Product9",1,Boards.BOARD1);
        Product p10 = new Product(String.valueOf("Product10".hashCode()),"Product10",11,Boards.BOARD1);

        //Do sth with them...
        list.addFront(p1);
        list.addFront(p2);
        list.addFront(p3);
        list.addFront(p4);
        list.addFront(p5);
        list.addFront(p6);
        list.addFront(p7);
        list.addFront(p8);
        list.addFront(p9);
        list.addRear(p10);

        list.printAll();
    }
}
