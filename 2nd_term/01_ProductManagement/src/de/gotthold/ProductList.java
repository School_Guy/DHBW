package de.gotthold;

public class ProductList {
    ListElement head;

    public ProductList() {
        head = new ListEnd();
    }

    public void addFront(Product p) {
        ListKnot newHead = new ListKnot(p);
        newHead.setSuccessor(head);
        head = newHead;
    }

    public void addRear(Product p) {
        head.addRear(p);
    }

    public void delete(Product p) {
        if (!(head.getProduct().equals(p))) {
            head = head.getSuccessor();
        } else {
            head.delete(p);
        }
    }

    public void printAll() {
        head.printList();
    }
}
