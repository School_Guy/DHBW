package de.gotthold;

public class Product {
    String uid;
    String name;
    int nettoprice;
    Boards boardnumber;

    public Product(String uid, String name, int nettoprice, Boards boardnumber) {
        this.uid = uid;
        this.name = name;
        this.nettoprice = nettoprice;
        this.boardnumber = boardnumber;
    }

    public String print() {
        return "uid:" + uid + " name:" + name + " nettoprice:" + nettoprice + " boardnumber:" + boardnumber.toString();
    }
}
