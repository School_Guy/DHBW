package de.gotthold;

public class Cell<T> {
    T data;
    Cell<T> next;

    public Cell(T o) {
        data = o;
    }

    public void printAll() {
        System.out.println(data.toString());
        if (next != null) {
            next.printAll();
        }
    }

    public void delete(T o) {
        if (next.data.equals(data)) {
            next = next.next;
        } else {
            if (next != null) {
                next.delete(o);
            }
        }
    }
}
