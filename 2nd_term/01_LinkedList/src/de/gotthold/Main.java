package de.gotthold;

public class Main {

    public static void main(String[] args) {
        VerketteteListe<TestClass> verketteteListe = new VerketteteListe<TestClass>();

        //Create objects
        TestClass test1 = new TestClass();
        TestClass test2 = new TestClass();
        TestClass test3 = new TestClass();
        TestClass test4 = new TestClass();
        TestClass test5 = new TestClass();
        TestClass test6 = new TestClass();
        TestClass test7 = new TestClass();
        TestClass test8 = new TestClass();
        TestClass test9 = new TestClass();
        TestClass test10 = new TestClass();

        //Add objects
        verketteteListe.add(test1);
        verketteteListe.add(test2);
        verketteteListe.add(test3);
        verketteteListe.add(test4);
        verketteteListe.add(test5);
        verketteteListe.add(test6);
        verketteteListe.add(test7);
        verketteteListe.add(test8);
        verketteteListe.add(test9);
        verketteteListe.add(test10);

        //Further work
        verketteteListe.printAll();
    }
}
