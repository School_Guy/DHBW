package de.gotthold;

public class VerketteteListe<T> {

    private Cell<T> anker; // Zeigt auf erstes Element
    private Cell<T> ende; // Zeigt auf letztes Element

    public void add(T o) {
        Cell<T> neu = new Cell<T>(o);
        if (ende == null) {
            anker = neu;
            ende = neu;
        } else {
            ende.next = neu;
            ende = neu;
        }
    }

    public void printAll() {
        anker.printAll();
    }

    public void delete(T o) {
        anker.delete(o);
    }
}