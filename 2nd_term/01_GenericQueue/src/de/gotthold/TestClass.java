package de.gotthold;

public class TestClass {

    public int property;
    public String property2;

    public TestClass() {}

    @Override
    public String toString() {
        return "TestClass{" +
                "property=" + property +
                ", property2='" + property2 + '\'' +
                '}';
    }
}
