package de.gotthold;

public class Main {

    public static void main(String[] args) {
        GenericArrayQueue<TestClass> testClassGenericArrayQueue = new GenericArrayQueue<TestClass>(TestClass.class);

        //Create objects
        TestClass test1 = new TestClass();
        TestClass test2 = new TestClass();
        TestClass test3 = new TestClass();
        TestClass test4 = new TestClass();
        TestClass test5 = new TestClass();
        TestClass test6 = new TestClass();
        TestClass test7 = new TestClass();
        TestClass test8 = new TestClass();
        TestClass test9 = new TestClass();
        TestClass test10 = new TestClass();
        TestClass test11 = new TestClass();

        //Add objects
        testClassGenericArrayQueue.add(test1);
        testClassGenericArrayQueue.add(test2);
        testClassGenericArrayQueue.add(test3);
        testClassGenericArrayQueue.add(test4);
        testClassGenericArrayQueue.add(test5);
        testClassGenericArrayQueue.add(test6);
        testClassGenericArrayQueue.add(test7);
        testClassGenericArrayQueue.add(test8);
        testClassGenericArrayQueue.add(test9);
        testClassGenericArrayQueue.add(test10);
        testClassGenericArrayQueue.add(test11);

        testClassGenericArrayQueue.printAll();
    }
}
