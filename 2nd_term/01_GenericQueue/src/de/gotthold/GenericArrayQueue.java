package de.gotthold;

import java.lang.reflect.Array;
import java.util.Arrays;

public class GenericArrayQueue<T> extends Queue<T> {
    private T[] inner;
    private int count;
    private Class<T> type;

    //see: https://stackoverflow.com/questions/529085/how-to-create-a-generic-array-in-java
    public GenericArrayQueue(Class<T> c) {
        this.type = c;
        @SuppressWarnings("unchecked") T[] a = (T[]) Array.newInstance(type, 100);
        this.inner = a;
        count = 0;
    }

    public void add(T object) {
        if (inner.length < size() + 1) {
            inner = Arrays.copyOf(inner, inner.length * 2);
        }
        inner[size()] = object;
        count++;
    }

    @Override
    public T take() {
        if (!isEmpty()) {
            T res = inner[size()];
            inner[size()] = null;
            rotate();
            count--;
            return res;
        } else {
            return null;
        }
    }

    private void rotate() {
        for (int i = 0; i < 99; i++) {
            inner[i] = inner[i + 1];
        }
        inner[99] = null;
    }

    @Override
    public T head() {
        return inner[0];
    }

    @Override
    public boolean isEmpty() {
        return size() > 0;
    }

    @Override
    public void printAll() {
        for (T t:inner) {
            if (t != null) {
                System.out.println(t.toString());
            }
        }
    }

    public int size() {
        return count;
    }

}
