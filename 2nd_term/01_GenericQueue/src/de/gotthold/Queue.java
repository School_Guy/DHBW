package de.gotthold;

public abstract class Queue<T> {
    public abstract void add(T t);
    public abstract T take();
    public abstract T head();
    public abstract boolean isEmpty();
    public abstract void printAll();
}
