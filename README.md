# DHBW

## About

This repository symbolises all code that I have written during my time at the
[DHBW Horb](https://www.dhbw-stuttgart.de/horb/) campus.

## History of the repo

The projects were previously multiple small repos.

Thanks to [Stackoverflow](https://stackoverflow.com/a/14470212) I was able to merge them into this "archive" monorepo.
