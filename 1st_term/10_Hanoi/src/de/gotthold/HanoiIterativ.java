package de.gotthold;

public class HanoiIterativ {

    private int n;

    public HanoiIterativ () {
        n = 5;
        int limit = (1 << n) - 1; // number of iterations = 2^n - 1
        for (int i = 0; i < limit; i++) {
            int d = disk(i); // disk to be moved
            int source = (movements(i, d) * direction(d)) % 3; // tower it is
            // currently
            // occupying
            int dest = (source + direction(d)) % 3; // tower to move it to
            out(d, source, dest);
        }
        System.out.println("Fertig.");
    }

    // disk that will be moved in step i
    int disk(int i) {
        int g, x = i + 1;
        for (g = 0; x % 2 == 0; g++)
            x /= 2;
        return g;
    }

    // how many times disk d is moved before stage i
    int movements(int i, int d) {
        return ((i >> d) + 1) >> 1;
    }

    // disk d always moves in the same direction
    // clockwise=1, counterclockwise=2
    int direction(int d) {
        return 2 - (n + d) % 2;
    }

    void out(int d, int source, int dest) {
        System.out.println("Moving disk " + (d+1) + " from tower " + source
                + " to tower " + dest);
    }
}
