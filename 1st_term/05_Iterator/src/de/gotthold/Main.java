package de.gotthold;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<Integer> zahlen = new LinkedList<Integer>();
        zahlen.add(5);
        zahlen.add(74);
        zahlen.add(58);
        zahlen.add(87);
        zahlen.add(37);
        zahlen.add(4);
        zahlen.add(35);
        zahlen.add(22);

        //Ungerade Zahlen entfernen
        /*Iterator<Integer> iterator = zahlen.iterator();
        while (iterator.hasNext()) {
            if (!(iterator % 2) == 0) {
                iterator.remove();
            }
        }*/

        //Johannes
        for(int i=0;i<zahlen.size();i++)
        {
            if ((zahlen.get(i) % 2 )!= 0)
            {
                zahlen.remove(i);
                i--; //aufgrund von löschen einer Zahl, springt Index schon eine Zahl indirekt weiter.
            }

        }
        System.out.println(zahlen.toString());
    }
}
