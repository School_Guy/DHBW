package de.gotthold;

public class Main {

    //https://stackoverflow.com/questions/7569335/reverse-a-string-in-java

    public static void main(String[] args) {
        String toCheck = "asdasdAbba";
        toCheck = toCheck.toLowerCase();
        String reversed = new StringBuilder(toCheck).reverse().toString();
        if (toCheck.equals(reversed)) {
            System.out.println(toCheck + " ist ein Palindrom.");
        } else {
            System.out.println(toCheck + " ist kein Palindrom.");
        }
    }
}
