package de.gotthold;

public class Main {

    public static void main(String[] args) {
        istPrimzahl(45);
    }

    public static boolean istPrimzahl(int p) {
        return istPrimzahl(p,p-1);
    }

    public static boolean istPrimzahl(int p, int z) {
        if (z==1) {
            return true;
        } else if (p%z==0) {
            return false;
        } else {
            return istPrimzahl(p,z-1);
        }
    }
}
