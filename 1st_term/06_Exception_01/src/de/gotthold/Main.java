package de.gotthold;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int number = -1;
        while (number == -1) {
            System.out.println("Please enter a number!");
            try {
                Scanner sc = new Scanner(System.in);
                while (true) {
                    if (sc.hasNext()) {
                        number = sc.nextInt();
                        break;
                    }
                }
                sc.close();
            } catch (Exception e) {
                System.out.println("Bitte eine Zahl");
            }
        }
        System.out.println(number * number);
    }
}
