package de.gotthold;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;

public class FileReader {
    private FileInputStream fileInputStream;
    private FileChannel fileChannel;
    private CharBuffer charBuffer;
    private char seperator = ',';
    private long filesize;

    public FileReader (File file, boolean big) {
        try {
            fileInputStream = new FileInputStream(file.getAbsolutePath());
            fileChannel = fileInputStream.getChannel();
            charBuffer = CharBuffer.allocate(big?104857600:1024);
            filesize = file.length();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Datei nicht gefunden.");
        }
    }

    public void readFile () {
        //for Schleife zum lesen
        //letztes Komma finden
        //Alles nach dem Komma vergessen
        //Position des Kommas merken
        //An den Counter übergeben
    }
}
