package de.gotthold;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Counter {
    //Sortieren nach erfolgreichem Zählen
    private HashMap<String,Integer> names;

    public Counter () {
        names = new HashMap<String, Integer>();
    }

    public void werteAus(){
        for(Map.Entry<String, Integer> entry : names.entrySet()) {
            String key = entry.getKey();
            Integer value = entry.getValue();

            // do what you have to do here
            // In your case, another loop.
        }
    }
}
