package de.gotthold;

import java.util.*;

public class Main {

    public static void main(String[] args) {
        //Treeset implementiert Navigable Set
        NavigableSet<String> s = new TreeSet<String>();
        s.add("b");
        s.add("a");
        s.add("d");
        s.add("c");
        Iterator<String> it = s.descendingIterator();
        while (it.hasNext()) {
            System.out.println("Value :" + it.next());
        }
    }
}
