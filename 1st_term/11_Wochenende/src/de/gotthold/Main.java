package de.gotthold;

import java.util.Calendar;

//Enno Gotthold Feb 2018

public class Main {

    public static void main(String[] args) {
        Calendar cal = Calendar.getInstance();

        Calendar calSummerHolidayBegin = Calendar.getInstance();
        calSummerHolidayBegin.set(Calendar.MONTH, 6);
        calSummerHolidayBegin.set(Calendar.DAY_OF_MONTH, 30);

        Calendar calSummerHolidayEnd = Calendar.getInstance();
        calSummerHolidayEnd.set(Calendar.MONTH, 9);
        calSummerHolidayEnd.set(Calendar.DAY_OF_MONTH, 10);
        for (int day = 1, maxDay = cal.getActualMaximum(Calendar.DAY_OF_YEAR); day <= maxDay; day++) {
            cal.set(Calendar.DAY_OF_YEAR, day);
            if (cal.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || cal.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
                //if (calSummerHolidayBegin.getTimeInMillis() - cal.getTimeInMillis() < 0
                //        && calSummerHolidayEnd.getTimeInMillis() - cal.getTimeInMillis() > 0) {
                //    System.out.println(cal.getTime());
                //}

                if (calSummerHolidayBegin.before(cal) && calSummerHolidayEnd.after(cal)) {
                    System.out.println(cal.getTime());
                }
            }
        }
    }
}
