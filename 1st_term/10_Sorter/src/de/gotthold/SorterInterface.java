package de.gotthold;

public interface SorterInterface {
    public int[] sort(int[] toSort);
}
