package de.gotthold;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] numbers = {5, 8, 1, 0, 9};
        System.out.println("MergeSort-Array unsortiert: " + Arrays.toString(numbers));
        SorterInterface mergeSort = new MergeSort();
        int[] result = mergeSort.sort(numbers);
        System.out.println("MergeSort-Array sortiert: " + Arrays.toString(result));

        int[] numbers2 = {5, 8, 1, 0, 9};
        System.out.println("QuickSort-Array unsortiert: " + Arrays.toString(numbers));
        SorterInterface quickSort = new QuickSort();
        result = quickSort.sort(numbers2);
        System.out.println("QuickSort-Array sortiert: " + Arrays.toString(result));
    }
}
