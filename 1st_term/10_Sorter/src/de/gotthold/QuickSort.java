package de.gotthold;

public class QuickSort implements SorterInterface {
    @Override
    public int[] sort(int[] toSort) {
        return quicksort(toSort, 0, toSort.length-1);
    }

    private int[] quicksort(int[] numbers, int low, int high) {
        int i = low, j = high;
        // Get the pivot element from the middle of the list
        int pivot = numbers[low + (high - low) / 2];

        // Divide into two lists
        while (i <= j) {
            while (numbers[i] < pivot) {
                i++;
            }
            while (numbers[j] > pivot) {
                j--;
            }
            if (i <= j) {
                //exchange(i, j);
                int temp = numbers[j];
                numbers[j] = numbers[i];
                numbers[i] = temp;

                i++;
                j--;
            }
        }

        // Recursion
        if (low < j)
            quicksort(numbers, low, j);
        if (i < high)
            quicksort(numbers, i, high);

        return numbers;
    }

}
