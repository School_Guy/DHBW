package de.gotthold;

import java.time.LocalDate;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
	    long milliSeconds = System.currentTimeMillis();
	    long birthdayMilliSeconds = TimeUnit.DAYS.toMillis(LocalDate.of(1998,12,6).toEpochDay());
	    long result = TimeUnit.MILLISECONDS.toMinutes(milliSeconds - birthdayMilliSeconds);
	    System.out.println(result);

	    System.out.println(TimeUnit.MILLISECONDS.toMinutes(System.currentTimeMillis() - TimeUnit.DAYS.toMillis(LocalDate.of(1998,12,6).toEpochDay())));
    }
}
