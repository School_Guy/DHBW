package de.gotthold;

public class Main {

    public static void main(String[] args) {
        String res1 = testTryCatchFinally(0);
        String res2 = testTryCatchFinally(-1);
        System.out.println("Erstes Resultat: " + res1);
        System.out.println("Zweites Resultat: " + res2);
    }

    public static String testTryCatchFinally(int number) {
        try {
            if (number < 0) {
                throw new ArithmeticException();
            }
            return "try";
        } catch (Exception e) {
            return "catch";
        } finally {
            System.out.println("finally");
        }
    }
}
