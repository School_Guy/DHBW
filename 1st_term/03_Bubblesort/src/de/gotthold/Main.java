package de.gotthold;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {56,42,35,84,36,147,69,257,623};
        printArray(array);
        Sorter bubbleSort = new BubbleSort();
        int[]res = bubbleSort.sort(array);
        printArray(res);
    }

    public static void printArray (int[] array) {
        System.out.print("[");
        for (int i: array) {
            System.out.print(i + " ");
        }
        System.out.print("]" + "\n");
    }
}
