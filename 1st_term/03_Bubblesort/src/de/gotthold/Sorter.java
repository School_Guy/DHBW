package de.gotthold;

public interface Sorter {
    public int[] sort (int[] array);
}
