package de.gotthold;

public class Bruch implements Comparable<Bruch> {
    int zaehler;
    int nenner;

    public Bruch() {
        this.zaehler = 1;
        this.nenner = 1;
        this.kuerzen();
    }

    public Bruch(int zaehler, int nenner) {
        this.zaehler = zaehler;
        if (nenner != 0) {
            this.nenner = nenner;
        } else { // Fehlerfall; wie man dies korrekt behandelt folgt spaeter
            this.nenner = 1;
        }
        this.kuerzen();
    }

    private int abs(int zahl) {
        if (zahl >= 0)
            return zahl;
        return -1 * zahl;
    }

    private int ggt(int zahl1, int zahl2) {
        if (zahl1 >= zahl2) {
            if (zahl1 % zahl2 == 0)
                return zahl2;
            else
                return ggt(zahl2, zahl1 - zahl2);
        } else
            return ggt(zahl2, zahl1);
    }

    private void kuerzen() {
        if (this.zaehler == 0) {
            this.nenner = 1;
            return;
        }
        int kuerz_faktor = ggt(abs(this.zaehler), abs(this.nenner));
        this.zaehler /= kuerz_faktor;
        this.nenner /= kuerz_faktor;
        if (this.zaehler < 0) {
            this.zaehler = -this.zaehler;
            this.nenner = -this.nenner;
        }
    }

    @Override
    public int compareTo(Bruch b) {
        //0 wenn gleich,-1 wenn kleiner und 1 wenn größer
        if (b.nenner == this.nenner & b.zaehler == this.zaehler) {
            return 0;
        } else if (this.zaehler / this.nenner > b.zaehler / b.nenner) {
            return -1;
        } else {
            return 1;
        }
    }
}
