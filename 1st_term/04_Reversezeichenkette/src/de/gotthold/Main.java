package de.gotthold;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.next();
        StringBuilder result = new StringBuilder();
        for (int i=text.length()-1;i>=0;i--) {
            result.append(text.charAt(i));
        }
        System.out.println("Text: " + text + "Ergebnis: " + result.toString());
    }
}
