package de.gotthold;

import java.io.IOException;
import java.io.Reader;

public class SwearWordEreaser extends Reader {

    private Reader in;

    public SwearWordEreaser(Reader in) {
        super();
        this.in = in;
    }

    @Override
    public int read(char[] cbuf, int off, int len) throws IOException {
        in.read(cbuf, off, len);
        for (int i = 0; i < cbuf.length; i++) {
            try {
                if (cbuf[i] == 'f' && cbuf[i + 1] == 'u' && cbuf[i + 2] == 'c' && cbuf[i + 3] == 'k') {
                    cbuf[i] = '*';
                    cbuf[i + 1] = '*';
                    cbuf[i + 2] = '*';
                    cbuf[i + 3] = '*';
                    i = i + 3;
                }
            } catch (IndexOutOfBoundsException e) {
                break;
            }
        }
        return 0;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
