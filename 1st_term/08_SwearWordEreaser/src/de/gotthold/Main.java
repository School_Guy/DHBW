package de.gotthold;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) {
        char[] chars = new char[1000];
        int length = 0;
        RandomWordsReader reader = new RandomWordsReader();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        LineBreakAdder lineBreakAdder = new LineBreakAdder(bufferedReader);
        SwearWordEreaser swearWordEreaser = new SwearWordEreaser(bufferedReader);
        try {
            length = reader.read(chars, 0, 0);
            printCharArr(chars);
            lineBreakAdder.read(chars,0,0);
            printCharArr(chars);
            swearWordEreaser.read(chars,0,0);
            printCharArr(chars);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void printCharArr (char[] arr) {
        for (int i = 0;i<arr.length;i++) {
            System.out.print(arr[i]);
        }
        System.out.println("\n-----------------------------------------");
    }
}
