package de.gotthold;

import de.gotthold.interfaces.SplitterInterface;

import java.io.*;

public class FileSplitter implements SplitterInterface {
    DataInputStream dataInputStream;
    DataOutputStream dataOutputStream;
    File file;

    public FileSplitter(File file) {
        try {
            this.file = file;
            dataInputStream = new DataInputStream(new FileInputStream(file));
            read(file);
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        }
    }

    public void split() {

    }

    private void read(File file) {
        int size = 1440000;
        byte[] buffer = new byte[size];
        int position = 0;
        int res = 0;
        do {
            try {
                res = dataInputStream.read(buffer, 0, size);
                position = position + size;
                if (res > 0) {
                    write(buffer, position / size, res);
                }
            } catch (EOFException eof) {
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (res > 0);
    }

    private void write(byte[] data, int position, int length) {
        try {
            File res = new File(this.file.getAbsolutePath() + this.file.getName() + "." + position);
            System.out.println("data.length: " + data.length + " lenght: " + length);
            dataOutputStream = new DataOutputStream(new FileOutputStream(res));
            dataOutputStream.write(data, 0, length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
