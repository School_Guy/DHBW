package de.gotthold;

import de.gotthold.interfaces.SplitterInterface;

import java.io.File;

public class Splitter {

    File file;
    SplitterInterface splitter;

    public Splitter(String arg) {
        file = new File(arg);
        if (file.isDirectory()) {
            splitter = new DirectorySplitter(file);
        } else if (file.isFile()) {
            splitter = new FileSplitter(file);
        } else {
            System.out.println("Argument ist weder Datei noch Pfad. Programm wird ohne Änderungen beendet!");
            System.out.println(file.getAbsolutePath());
        }
    }
}
