package de.gotthold;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class FileSplitter implements SplitterInterface {
    FileInputStream fileInputStream;
    FileChannel fileChannelIn;
    FileOutputStream fileOutputStream;
    FileChannel fileChannelOut;
    File file;

    public FileSplitter(File file) {
        try {
            this.file = file;
            fileInputStream = new FileInputStream(file);
            fileChannelIn = fileInputStream.getChannel();
            read(file);
        } catch (FileNotFoundException e) {
            System.out.println("File Not Found");
        }
    }

    public void split() {

    }

    private void read(File file) {
        //int size = 1073741824;
        //int size = 524288000;
        int size = 1024;
        //Eigentlich ByteBuffer[], aber ByteBuffer
        ByteBuffer buffer = ByteBuffer.allocate(size);
        long position = 0;
        long res = 0;
        do {
            try {
                System.out.println("Lesen");
                res = fileChannelIn.read(buffer, 0, size);
                position = position + size;
                if (res > 0) {
                    write(buffer, position / size, res);
                }
            } catch (EOFException eof) {
                break;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (res > 0);
    }

    private void write(ByteBuffer[] data, long position, long length) {
        try {
            File res = new File(this.file.getAbsolutePath() + this.file.getName() + "." + position);
            //.length ist falsch
            System.out.println("data.length: " + data.length + " lenght: " + length);
            fileOutputStream = new FileOutputStream(res);
            fileChannelOut = fileOutputStream.getChannel();
            int temp = (int) length;
            fileChannelOut.write(data, 0, temp);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
