package de.gotthold;

public class Main {

    public static void main(String[] args) {
        CliParser cliParser = new CliParser();
        cliParser.parse(args);
    }
}
