package de.gotthold;

public class CliParser {

    //TODO: Hardgecodete Abhängigkeit zur Konsole herauslösen

    public void parse(String[] args) {
        if (args.length == 0) {
            System.out.println("Keine Argumente angegeben! Bitte erneut mit Argumenten versuchen.");
        } else if (args.length > 1) {
            System.out.println("Zu viele Argumente angegeben! Bitte erneut mit einem Argument versuchen.");
        } else {
            Splitter splitter = new Splitter(args[0]);
        }
    }
}
