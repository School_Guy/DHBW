package de.gotthold;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        moeglichkeit1();
        System.out.println("----------------");
        moeglichkeit2();
    }

    public static void moeglichkeit1 () {
        List a = new ArrayList();
        List b = a;
        a.add("hallo");
        System.out.println(b.size());
    }

    public static void moeglichkeit2() {
        List a = new ArrayList();
        List b = new ArrayList();
        a.add("hallo");
        System.out.println(b.size());
    }
}
