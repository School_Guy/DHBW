package de.gotthold;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] arr = {5,4,1,8,9,6,2,7,0,3};
        System.out.println("Grundarray: " + Arrays.toString(arr));

        Sorter sorter = new HeapSort();
        int[] res = sorter.sort(arr);

        System.out.println("Resultat: " + Arrays.toString(res));
    }
}
