package de.gotthold;

import java.util.Arrays;

public class HeapSort implements Sorter {

    private int[] a;

    @Override
    public int[] sort(int[] toSort) {
        a = toSort;
        baueHeap();
        System.out.println("Heap initial aufgebaut: " + Arrays.toString(a));
        for (int i = a.length - 1; i > 0; i--) {
            tausche(0, i);
            heapify(0, i - 1);
        }
        return a;
    }

    private void heapify(int dieserKnoten, int heapGroesse) {
        int links = 2 * dieserKnoten;
        int rechts = links + 1;
        if (links <= heapGroesse && rechts > heapGroesse) {
            if (a[dieserKnoten] > a[links]) {
                tausche(dieserKnoten, links);
            }
        } else if (rechts <= heapGroesse) {
            int derSohn = (a[links] < a[rechts] ? links : rechts);
            if (a[dieserKnoten] > a[derSohn]) {
                tausche(dieserKnoten, derSohn);
                heapify(derSohn, heapGroesse);
            }
        }
    }

    private void tausche(int index1, int index2) {
        int temp = a[index1];
        a[index1] = a[index2];
        a[index2] = temp;
        index1++;
        index2++;
        System.out.println("Knoten " + index1 + " und " + index2 + " getauscht: " + Arrays.toString(a));
    }

    private void baueHeap() {
        for (int i = (a.length - 1) / 2; i >= 0; i--) {
            heapify(i, a.length - 1);
        }
    }
}
